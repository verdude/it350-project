##Java
1. Open a terminal (ctrl +_ alt + t)
```bash
sudo apt-get -y install openjdk-7-jdk
which java
java -version
```
##Download play-2.1.0
```bash
cd ~
sudo wget http://downloads.typesafe.com/play/2.1.0/play-2.1.0.zip
sudo unzip play-2.1.0.zip
```
* The next line is necessary to run play as an env variable as a normal user otherwise you would need to update the super user path which would only be accessible when in super user mode (sudo su). If you choose to skip the environment variable, you need to specify the full or relative path to the play bash script when running the application.
```bash
sudo chmod -R 777 ~/play-2.1.0
# add export PATH=$PATH:~/play-2.1.0
vi ~/.bashrc
# apply the differences to the path
source ~/.bashrc
```

##LAMP
```bash
sudo apt-get -y install tasksel
sudo tasksel
```
* Then select LAMP server
* Navigate with the arrow keys and select with the space bar
* Press enter to install
* Create passwords as you are prompted

##Setting up the database
1. Open a terminal
```bash
mysql -u root -p
```
2. Enter your password
```SQL
CREATE DATABASE shop;
exit
```

##Create the app
1. Open a terminal
```bash
play new appName
```
2. Press enter
3. Press 1 and enter (This will set the app to use scala)

**If you want to use our code rather than create your own, skip the create the app step and follow the clone the project step.**

##Clone the project
1. Open a terminal
2. If you do not have git installed,
```bash
sudo apt-get install git
```
3. Then,
```bash
git clone https://github.com/verdude/IT350-Project.git
```
4. Then navigate to the conf/ directory of the app and open the application.conf.example file in an editor. Change the value of db.default.password="" to your database's password.
5. Delete the .example extension of the application.conf.example file.

##Run the application
1. In a terminal, switch to the directory where your app is stored.
```bash
play run
```
Play will automatically download any necessary dependencies. Then it will start the app on the default port 9000.
2. Open a browser and navigate to localhost:9000.
