#! /bin/bash

# run this on a new ubuntu 14.04 install with sudo

# java
sudo apt-get -y install openjdk-7-jdk

# play
cd ~
sudo wget http://downloads.typesafe.com/play/2.1.0/play-2.1.0.zip
sudo unzip play-2.1.0.zip

# in order to create a path variable that won't affect the root user

# user permissions and path
sudo chmod -R 777 ~/play-2.1.0
# user path update
sudo echo "\nexport PATH=$PATH:~/play-2.1.0" >> ~/.bashrc
# apply changes
sudo source ~/.bashrc

# installs 'lamp'
# not sure if this includes phpmyadmin
sudo apt-get update
sudo apt-get -y install apache2
# ifconfig eth0 | grep inet | awk '{ print $2 }'
sudo apt-get -y install mysql-server
sudo mysql_secure_installation
sudo apt-get -y install php5 php-pear php5-mysql
sudo service apache2 restart

# This is assuming that there is no mysql password set
# and access without a password is enabled

# create database
mysql -u root -e "CREATE DATABASE shop;"

# git
sudo apt-get -y install git
git clone https://github.com/verdude/IT350-Project.git
cd IT350-Project

# make config file
sudo cp conf/application.conf.example conf/application.conf

# start app
play run
