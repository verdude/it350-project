package models

import anorm.{SQL, ~}
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current

case class ClassPayment(cPayId: Option[Long], storeId: Long, paymentType: String, amount: Double, pay_date: String) {

    /**
     * save a classPayment in the database
     */
    def save: ClassPayment = {
        if (!cPayId.isEmpty) {
            //update the classPayment
            play.Logger.debug("Update classPayment info not implemented.")
            this
        } else {
            //insert
            DB.withConnection {
                implicit connection =>
                    // This may need to be an Int
                    val cPayId: Option[Long] = SQL("insert into " + ClassPayment.tableName + 
                        " (storeId, paymentType, amount, pay_date) values " +
                        "({storeId}, {paymentType}, {amount}, {pay_date})")
                        .on('storeId -> this.storeId, 'paymentType -> this.paymentType,'amount -> this.amount, 'pay_date -> this.pay_date.toLowerCase)
                        .executeInsert()
                    play.Logger.info("inserted classPayment Id: " + cPayId.getOrElse("Funky Monkies! There is no cPayId"))
                    this.copy(cPayId)
            }
        }
    }

    /**
     * remove a classPayment from the db by it's id
     */
    def delete = {
        if (!cPayId.isEmpty) {
            DB.withConnection {
                implicit connection =>
                    val affected = SQL("delete from " + ClassPayment.tableName + " where cPayId = {sid}")
                        .on('sid -> this.cPayId).executeUpdate()
            }
        } else {
            play.Logger.error("Cannot Delete ClassPayment Without Id")
        }
    }

}

object ClassPayment {
    val tableName = "classPayment"

    /**
     * Parses a result set row into a classPayment object
     */
    val simple = {
        get[Option[Long]](tableName + ".cPayId") ~
        get[Long](tableName + ".storeId") ~
        get[String](tableName + ".paymentType") ~
        get[Double](tableName + ".amount") ~
        get[String](tableName + ".pay_date") map {
            case cPayId ~ storeId ~ paymentType ~ amount ~ pay_date => {
                models.ClassPayment(cPayId, storeId, paymentType, amount, pay_date)
            }
        }
    }

    /**
     * Gets all the classPayments in the database
     */
    def list: List[ClassPayment] = {
        DB.withConnection {
            implicit connection =>
                SQL("select * from " + tableName).as(simple*)
        }
    }
}