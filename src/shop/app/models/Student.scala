package models

import anorm.{SQL, ~}
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current

case class Student(studentId: Option[Long], student_name: String, student_phone: String, student_dob: String) {

    /**
     * save a Student in the database
     */
    def save: Student = {
        if (!studentId.isEmpty) {
            //update the student
            play.Logger.debug("Update Student info not implemented.")
            this
        } else {
            //insert
            DB.withConnection {
                implicit connection =>
                    // This may need to be an Int
                    val studentId: Option[Long] = SQL("insert into " + Student.tableName + 
                        " (student_name, student_phone, student_dob) values " +
                        "({student_name}, {student_phone}, {student_dob})")
                        .on('student_name -> this.student_name.toLowerCase, 'student_phone -> this.student_phone, 'student_dob -> this.student_dob)
                        .executeInsert()
                    play.Logger.info("inserted Student Id: " + studentId.getOrElse("Funky Monkies! There is no studentId"))
                    this.copy(studentId)
            }
        }
    }

    /**
     * remove a student from the db by it's id
     */
    def delete = {
        if (!studentId.isEmpty) {
            DB.withConnection {
                implicit connection =>
                    val affected = SQL("delete from " + Student.tableName + " where studentId = {pid}")
                        .on('pid -> this.studentId).executeUpdate()
            }
        } else {
            play.Logger.error("Cannot Delete Student Without Id")
        }
    }

}

object Student {
    val tableName = "student"

    /**
     * Parses a result set row into a student object
     */
    val simple = {
        get[Option[Long]](tableName + ".studentId") ~
        get[String](tableName + ".student_dob") ~
        get[String](tableName + ".student_name") ~
        get[String](tableName + ".student_phone") map {
            case studentId ~ student_dob ~ student_name ~ student_phone => {
                models.Student(studentId, student_name, student_phone, student_dob)
            }
        }
    }

    /**
     * Gets all the students in the database
     */
    def list: List[Student] = {
        DB.withConnection {
            implicit connection =>
                SQL("select * from " + tableName).as(simple*)
        }
    }
}