package models

import anorm.{SQL, ~}
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current

case class Purchase(purchaseId: Option[Long], storeId: Long, purchase_date: String, productId: Long) {

    /**
     * save a purchase in the database
     */
    def save: Purchase = {
        if (!purchaseId.isEmpty) {
            //update the purchase
            play.Logger.debug("Update purchase info not implemented.")
            this
        } else {
            //insert
            DB.withConnection {
                implicit connection =>
                    // This may need to be an Int
                    val purchaseId: Option[Long] = SQL("insert into " + Purchase.tableName + 
                        " (storeId, purchase_date, productId) values " +
                        "({storeId}, {purchase_date}, {productId})")
                        .on('storeId -> this.storeId, 'purchase_date -> this.purchase_date.toLowerCase,'productId -> this.productId)
                        .executeInsert()
                    play.Logger.info("inserted purchase Id: " + purchaseId.getOrElse("Funky Monkies! There is no purchaseId"))
                    this.copy(purchaseId)
            }
        }
    }

    /**
     * remove a purchase from the db by it's id
     */
    def delete = {
        if (!purchaseId.isEmpty) {
            DB.withConnection {
                implicit connection =>
                    val affected = SQL("delete from " + Purchase.tableName + " where purchaseId = {sid}")
                        .on('sid -> this.purchaseId).executeUpdate()
            }
        } else {
            play.Logger.error("Cannot Delete Purchase Without Id")
        }
    }

}

object Purchase {
    val tableName = "purchase"

    /**
     * Parses a result set row into a purchase object
     */
    val simple = {
        get[Option[Long]](tableName + ".purchaseId") ~
        get[Long](tableName + ".storeId") ~
        get[String](tableName + ".purchase_date") ~
        get[Long](tableName + ".productId") map {
            case purchaseId ~ storeId ~ purchase_date ~ productId => {
                models.Purchase(purchaseId, storeId, purchase_date, productId)
            }
        }
    }

    /**
     * Gets all the purchases in the database
     */
    def list: List[Purchase] = {
        DB.withConnection {
            implicit connection =>
                SQL("select * from " + tableName).as(simple*)
        }
    }
}