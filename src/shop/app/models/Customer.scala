package models

import anorm.{SQL, ~}
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current

case class Customer(customerId: Option[Long], customer_phone: String, customer_name: String, customer_addr: String, customer_email: String) {

    /**
     * save a customer in the database
     */
    def save: Customer = {
        if (!customer_phone.isEmpty) {
            //update the customer
            play.Logger.debug("Update customer info not implemented.")
            this
        } else {
            //insert
            DB.withConnection {
                implicit connection =>
                    // This may need to be an Int
                    val customer_phone: Option[Long] = SQL("insert into " + Customer.tableName + 
                        " (customer_phone, customer_name, customer_addr, customer_email) values " +
                        "({customer_phone}, {customer_name}, {customer_addr}, {customer_email})")
                        .on('customer_name -> this.customer_name.toLowerCase, 'customer_addr -> this.customer_addr.toLowerCase
                            ,'customer_email -> this.customer_email.toLowerCase)
                        .executeInsert()
                    play.Logger.info("inserted customer Id: " + customer_phone.getOrElse("Funky Monkies! There is no customer_phone"))
                    this.copy(customer_phone)
            }
        }
    }

    /**
     * remove a customer from the db by it's id
     */
    def delete = {
        if (!customerId.isEmpty) {
            DB.withConnection {
                implicit connection =>
                    val affected = SQL("delete from " + Customer.tableName + " where customerId = {sid}")
                        .on('sid -> this.customerId).executeUpdate()
            }
        } else {
            play.Logger.error("Cannot Delete Customer Without Id")
        }
    }

}

object Customer {
    val tableName = "customer"

    /**
     * Parses a result set row into a customer object
     */
    val simple = {
        get[Option[Long]](tableName + ".customerId") ~
        get[String](tableName + ".customer_phone") ~
        get[String](tableName + ".customer_name") ~
        get[String](tableName + ".customer_addr") ~
        get[String](tableName + ".customer_email") map {
            case customerId ~ customer_phone ~ customer_name ~ customer_addr ~ customer_email => {
                models.Customer(customerId, customer_phone, customer_name, customer_addr, customer_email)
            }
        }
    }

    /**
     * Gets all the customers in the database
     */
    def list: List[Customer] = {
        DB.withConnection {
            implicit connection =>
                SQL("select * from " + tableName).as(simple*)
        }
    }
}