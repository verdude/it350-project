package models

import anorm.{SQL, ~}
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current

case class Store(storeId: Option[Long], manager: String, address: String, store_phone: String) {

    /**
     * save a store in the database
     */
    def save: Store = {
        if (!storeId.isEmpty) {
            //update the store
            play.Logger.debug("Update store info not implemented.")
            this
        } else {
            //insert
            DB.withConnection {
                implicit connection =>
                    // This may need to be an Int
                    val storeId: Option[Long] = SQL("insert into " + Store.tableName + 
                        " (manager, address, store_phone) values " +
                        "({manager}, {address}, {store_phone})")
                        .on('manager -> this.manager.toLowerCase, 'address -> this.address.toLowerCase,'store_phone -> this.store_phone)
                        .executeInsert()
                    play.Logger.info("inserted store Id: " + storeId.getOrElse("Funky Monkies! There is no storeId"))
                    this.copy(storeId)
            }
        }
    }

    /**
     * remove a store from the db by it's id
     */
    def delete = {
        if (!storeId.isEmpty) {
            DB.withConnection {
                implicit connection =>
                    val affected = SQL("delete from " + Store.tableName + " where storeId = {sid}")
                        .on('sid -> this.storeId).executeUpdate()
            }
        } else {
            play.Logger.error("Cannot Delete Store Without Id")
        }
    }

}

object Store {
    val tableName = "store"

    /**
     * Parses a result set row into a store object
     */
    val simple = {
        get[Option[Long]](tableName + ".storeId") ~
        get[String](tableName + ".manager") ~
        get[String](tableName + ".address") ~
        get[String](tableName + ".store_phone") map {
            case storeId ~ manager ~ address ~ store_phone => {
                models.Store(storeId, manager, address, store_phone)
            }
        }
    }

    /**
     * Gets all the stores in the database
     */
    def list: List[Store] = {
        DB.withConnection {
            implicit connection =>
                SQL("select * from " + tableName).as(simple*)
        }
    }
}