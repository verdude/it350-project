package models

import anorm.{SQL, ~}
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current
import dataAccess.Selectable

case class Login(loginId: Option[Long], name: String, username: String, password: String, position: String) {

  /**
   * save a login in the database
   */
  def save: Login = {
    if (!loginId.isEmpty) {
      //update the login
      play.Logger.debug("Update login info not implemented.")
      this
    } else {
      //insert
      DB.withConnection {
        implicit connection =>
          // This may need to be an Int
          val loginId: Option[Long] = SQL("insert into " + Login.tableName + 
            " (name, username, password, position) values " +
            "({name}, {username}, {password}, {position})")
            .on('name -> this.name, 'username -> this.username,'password -> this.password, 'position -> this.position.toLowerCase)
            .executeInsert()
          play.Logger.info("inserted login Id: " + loginId.getOrElse("Funky Monkies! There is no loginId"))
          this.copy(loginId)
      }
    }
  }

  /**
   * remove a login from the db by it's id
   */
  def delete = {
    if (!loginId.isEmpty) {
      DB.withConnection {
        implicit connection =>
          val affected = SQL("delete from " + Login.tableName + " where loginId = {sid}")
            .on('sid -> this.loginId).executeUpdate()
      }
    } else {
      play.Logger.error("Cannot Delete Login Without Id")
    }
  }

}

object Login extends Selectable[Login] {
  val tableName = "login"
  val positions = Map(
    0 -> "user",
    1 -> "admin"
  )

  /**
   * Parses a result set row into a login object
   */
  val simple = {
    get[Option[Long]](tableName + ".loginId") ~
    get[String](tableName + ".name") ~
    get[String](tableName + ".username") ~
    get[String](tableName + ".password") ~
    get[String](tableName + ".position") map {
      case loginId ~ name ~ username ~ password ~ position => {
        models.Login(loginId, name, username, password, position)
      }
    }
  }

  def withUser(username: String, password: String): Option[Login] = {
    DB.withConnection {
      implicit connection =>
        val login = SQL("select * from " + tableName + " where username = {usr} and password = {pwd}")
          .on('usr -> username, 'pwd -> password).as(simple*)
        play.Logger.debug(login.toString)
        login match {
          case _:List[Login] if login.length > 0 => Some(login(0))
          case _ => None
        }
    }
  }

  def register(name: String, username: String, password: String) : Option[Login] = {
    DB.withConnection {
      implicit connection =>
        val loginExists = SQL("select * from " + tableName + " where username = {usr}")
          .on('usr -> username).as(simple*)
        loginExists match {
          case _:List[Login] if loginExists.length > 0 => None
          case _ => {
            Some(Login(None, name, username, password, positions(0)).save)
          }
        }
    }
  }
}