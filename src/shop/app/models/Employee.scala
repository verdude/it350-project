package models

import anorm.{SQL, ~}
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current

case class Employee(empId: Option[Long], manager: String, address: String, employee_phone: String) {

    /**
     * save a employee in the database
     */
    def save: Employee = {
        if (!empId.isEmpty) {
            //update the employee
            play.Logger.debug("Update employee info not implemented.")
            this
        } else {
            //insert
            DB.withConnection {
                implicit connection =>
                    // This may need to be an Int
                    val empId: Option[Long] = SQL("insert into " + Employee.tableName + 
                        " (manager, address, employee_phone) values " +
                        "({manager}, {address}, {employee_phone})")
                        .on('manager -> this.manager.toLowerCase, 'address -> this.address.toLowerCase,'employee_phone -> this.employee_phone)
                        .executeInsert()
                    play.Logger.info("inserted employee Id: " + empId.getOrElse("Funky Monkies! There is no empId"))
                    this.copy(empId)
            }
        }
    }

    /**
     * remove a employee from the db by it's id
     */
    def delete = {
        if (!empId.isEmpty) {
            DB.withConnection {
                implicit connection =>
                    val affected = SQL("delete from " + Employee.tableName + " where empId = {sid}")
                        .on('sid -> this.empId).executeUpdate()
            }
        } else {
            play.Logger.error("Cannot Delete Employee Without Id")
        }
    }

}

object Employee {
    val tableName = "employee"

    /**
     * Parses a result set row into a employee object
     */
    val simple = {
        get[Option[Long]](tableName + ".empId") ~
        get[String](tableName + ".manager") ~
        get[String](tableName + ".address") ~
        get[String](tableName + ".employee_phone") map {
            case empId ~ manager ~ address ~ employee_phone => {
                models.Employee(empId, manager, address, employee_phone)
            }
        }
    }

    /**
     * Gets all the employees in the database
     */
    def list: List[Employee] = {
        DB.withConnection {
            implicit connection =>
                SQL("select * from " + tableName).as(simple*)
        }
    }
}