package models

import anorm.{SQL, ~}
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current

case class Product(productId: Option[Long], quantity: Long, price: java.math.BigDecimal, name: String) {

    /**
     * save a Product in the database
     */
    def save: Product = {
        if (!productId.isEmpty) {
            //update the product
            play.Logger.debug("Update Product info not implemented.")
            this
        } else {
            //insert
            DB.withConnection {
                implicit connection =>
                    // This may need to be an Int
                    val productId: Option[Long] = SQL("insert into " + Product.tableName + 
                        " (quantity, price, name) values " +
                        "({quantity}, {price}, {name})")
                        .on('quantity -> this.quantity, 'price -> this.price, 'name -> this.name)
                        .executeInsert()
                    play.Logger.info("inserted Product Id: " + productId.getOrElse("Funky Monkies! There is no productId"))
                    this.copy(productId)
            }
        }
    }

    /**
     * remove a product from the db by it's id
     */
    def delete = {
        if (!productId.isEmpty) {
            DB.withConnection {
                implicit connection =>
                    val affected = SQL("delete from " + Product.tableName + " where productId = {pid}")
                        .on('pid -> this.productId).executeUpdate()
            }
        } else {
            play.Logger.error("Cannot Delete Product Without Id")
        }
    }

}

object Product {
    val tableName = "product"

    /**
     * Parses a result set row into a product object
     */
    val simple = {
        get[Option[Long]](tableName + ".productId") ~
        get[String](tableName + ".name") ~
        get[Long](tableName + ".quantity") ~
        get[java.math.BigDecimal](tableName + ".price") map {
            case productId ~ name ~ quantity ~ price => {
                models.Product(productId, quantity, price, name)
            }
        }
    }

    /**
     * Gets all the products in the database
     */
    def list: List[Product] = {
        DB.withConnection {
            implicit connection =>
                SQL("select * from " + tableName).as(simple*)
        }
    }

    //get all guitars
    def getByName(name: String): List[Product] = {
        DB.withConnection {
            implicit connection =>
                SQL("select * from " + tableName + " where name like {name}")
                    .on('name -> name).as(simple*)
        }
    }
}