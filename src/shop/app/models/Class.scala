package models

import anorm.{SQL, ~}
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current

case class Class(classId: Option[Long], storeId: Long, date_time: Double) {

    /**
     * save a Class in the database
     */
    def save: Class = {
        if (!classId.isEmpty) {
            //update the class
            play.Logger.debug("Update Class info not implemented.")
            this
        } else {
            //insert
            DB.withConnection {
                implicit connection =>
                    // This may need to be an Int
                    val classId: Option[Long] = SQL("insert into " + Class.tableName + 
                        " (storeId, price, date_time) values " +
                        "({storeId}, {price}, {date_time})")
                        .on('storeId -> this.storeId, 'date_time -> this.date_time)
                        .executeInsert()
                    play.Logger.info("inserted Class Id: " + classId.getOrElse("Funky Monkies! There is no classId"))
                    this.copy(classId)
            }
        }
    }

    /**
     * remove a class from the db by it's id
     */
    def delete = {
        if (!classId.isEmpty) {
            DB.withConnection {
                implicit connection =>
                    val affected = SQL("delete from " + Class.tableName + " where classId = {pid}")
                        .on('pid -> this.classId).executeUpdate()
            }
        } else {
            play.Logger.error("Cannot Delete Class Without Id")
        }
    }

}

object Class {
    val tableName = "class"

    /**
     * Parses a result set row into a class object
     */
    val simple = {
        get[Option[Long]](tableName + ".classId") ~
        get[Long](tableName + ".storeId") ~
        get[Double](tableName + ".date_time") map {
            case classId ~ storeId ~ date_time => {
                models.Class(classId, storeId, date_time)
            }
        }
    }

    /**
     * Gets all the classs in the database
     */
    def list: List[Class] = {
        DB.withConnection {
            implicit connection =>
                SQL("select * from " + tableName).as(simple*)
        }
    }
}