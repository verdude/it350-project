package models

import anorm.{SQL, ~}
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current

case class Supplier(supplierId: Option[Long], companyName: String, orderId: Long, supplier_addr: String) {

    /**
     * save a supplier in the database
     */
    def save: Supplier = {
        if (!supplierId.isEmpty) {
            //update the supplier
            play.Logger.debug("Update supplier info not implemented.")
            this
        } else {
            //insert
            DB.withConnection {
                implicit connection =>
                    // This may need to be an Int
                    val supplierId: Option[Long] = SQL("insert into " + Supplier.tableName + 
                        " (companyName, orderId, supplier_addr) values " +
                        "({companyName}, {orderId}, {supplier_addr})")
                        .on('companyName -> this.companyName, 'orderId -> this.orderId,'supplier_addr -> this.supplier_addr)
                        .executeInsert()
                    play.Logger.info("inserted supplier Id: " + supplierId.getOrElse("Funky Monkies! There is no supplierId"))
                    this.copy(supplierId)
            }
        }
    }

    /**
     * remove a supplier from the db by it's id
     */
    def delete = {
        if (!supplierId.isEmpty) {
            DB.withConnection {
                implicit connection =>
                    val affected = SQL("delete from " + Supplier.tableName + " where supplierId = {sid}")
                        .on('sid -> this.supplierId).executeUpdate()
            }
        } else {
            play.Logger.error("Cannot Delete Supplier Without Id")
        }
    }

}

object Supplier {
    val tableName = "supplier"

    /**
     * Parses a result set row into a supplier object
     */
    val simple = {
        get[Option[Long]](tableName + ".supplierId") ~
        get[String](tableName + ".companyName") ~
        get[Long](tableName + ".orderId") ~
        get[String](tableName + ".supplier_addr") map {
            case supplierId ~ companyName ~ orderId ~ supplier_addr => {
                models.Supplier(supplierId, companyName, orderId, supplier_addr)
            }
        }
    }

    /**
     * Gets all the suppliers in the database
     */
    def list: List[Supplier] = {
        DB.withConnection {
            implicit connection =>
                SQL("select * from " + tableName).as(simple*)
        }
    }
}