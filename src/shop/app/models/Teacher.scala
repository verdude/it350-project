package models

import anorm.{SQL, ~}
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current

case class Teacher(teacherId: Option[Long], storeId: Long, empId: Long, firstName: String, lastName: String) {

    /**
     * save a teacher in the database
     */
    def save: Teacher = {
        if (!teacherId.isEmpty) {
            //update the teacher
            play.Logger.debug("Update teacher info not implemented.")
            this
        } else {
            //insert
            DB.withConnection {
                implicit connection =>
                    // This may need to be an Int
                    val teacherId: Option[Long] = SQL("insert into " + Teacher.tableName + 
                        " (storeId, empId, firstName, lastName) values " +
                        "({storeId}, {empId}, {firstName}, {lastName})")
                        .on('storeId -> this.storeId, 'empId -> this.empId,'firstName -> this.firstName.toLowerCase, 'lastName -> this.lastName.toLowerCase)
                        .executeInsert()
                    play.Logger.info("inserted teacher Id: " + teacherId.getOrElse("Funky Monkies! There is no teacherId"))
                    this.copy(teacherId)
            }
        }
    }

    /**
     * remove a teacher from the db by it's id
     */
    def delete = {
        if (!teacherId.isEmpty) {
            DB.withConnection {
                implicit connection =>
                    val affected = SQL("delete from " + Teacher.tableName + " where teacherId = {sid}")
                        .on('sid -> this.teacherId).executeUpdate()
            }
        } else {
            play.Logger.error("Cannot Delete Teacher Without Id")
        }
    }

}

object Teacher {
    val tableName = "teacher"

    /**
     * Parses a result set row into a teacher object
     */
    val simple = {
        get[Option[Long]](tableName + ".teacherId") ~
        get[Long](tableName + ".storeId") ~
        get[Long](tableName + ".empId") ~
        get[String](tableName + ".firstName") ~
        get[String](tableName + ".lastName") map {
            case teacherId ~ storeId ~ empId ~ firstName ~ lastName => {
                models.Teacher(teacherId, storeId, empId, firstName, lastName)
            }
        }
    }

    /**
     * Gets all the teachers in the database
     */
    def list: List[Teacher] = {
        DB.withConnection {
            implicit connection =>
                SQL("select * from " + tableName).as(simple*)
        }
    }
}