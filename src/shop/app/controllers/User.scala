package controllers

import play.api._
import play.api.mvc._
import play.api.mvc.Results._
import models._

case class User(loginId: Long, name: String, username: String, position: String)

object UserManagement extends Controller {

  /**
   * Creates a user from a login object if the user exists
   */
  def getUserFromLogin(login: Login): Option[User] = {
    login.loginId match {
      case _:Option[Long] if login.loginId.get > -1 => Some(User(login.loginId.get, login.name, login.username, login.position))
      case _ => None
    }
  }

  /**
   * Gets the user from the session by id
   */
  def getUserFromSession(implicit request: RequestHeader): Option[User] = {
    val id = request.session.get("userId").getOrElse(-1).toString.toLong
    if (id > -1) {
      val login: Login = Login.findById(Login.tableName, id, Login.simple).get
      login.loginId match {
        case _:Option[Long] if login.loginId.get > -1 => Some(User(login.loginId.getOrElse(-1), login.name, login.username, login.position))
        case _ => None
      }
    } else None
  }

  /**
   * gets an the id from an Option[Login] object
   */
  private val getId = (x:Option[Login]) => x.get.loginId.getOrElse(-1).toString

  def logout = Action {
    Redirect(routes.Application.index()).withNewSession
  }

  /**
   * Authenticates a user
   */
  def login = Action(parse.urlFormEncoded) {
    request =>
      // need to encode the passwords before checking the database
      val username = request.body("username")(0)
      val password = request.body("password")(0)

      val login = Login.withUser(username, password)
      login match {
        // can't be passing the password back to the frontend.
        // need some new way of giving the user information back
        // So I made this user object without the password
        case Some(Login(_,_,username,password,_)) => {
          Redirect(routes.Application.index()).withSession("userId" -> getId(login))
        }
        case _ => Redirect(routes.Application.loginPage()).flashing("error" -> "Invalid Username Or Password.").withNewSession
      }    
  }

  /**
   * Creates a new user with the provided information
   */
  def register = Action(parse.urlFormEncoded) {
    implicit request =>
      val name = request.body("name")(0)
      val username = request.body("username")(0)
      val password = request.body("password")(0)
      val password2 = request.body("password2")(0)

      if (password == password2) {
        val login = Login.register(name, username, password)
        login match {
          case Some(Login(_,_,username,password,_)) => Redirect(routes.Application.index())
            .withSession("userId" -> getId(login))
          case _ => Ok(views.html.register()).flashing("alert" -> "Username already exists.")
        }
      } else {
        Ok(views.html.register()).flashing("alert" -> "Passwords Do Not Match.")
      }
  }
}