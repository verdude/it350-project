package controllers

import play.api._
import play.api.mvc._
import play.api.mvc.Results._
import models._

object Application extends Controller {
  
  def index = Action {
    implicit request =>
      Ok(views.html.index("Santi's OneStop"))
  }

  def loginPage = Action {
    implicit request =>
      Ok(views.html.login())
  }

  def registerPage = Action {
    implicit request =>
      Ok(views.html.register())
  }

  /**
   *  super insecure
   */
  def instruments(instrument: String) = Action {
    Ok(views.html.instruments(Product.getByName(instrument)))
  }

  def classes = Action {
    Ok
  }

  def location(id: Long) = Action {
    Ok
  }

  def locations = Action {
    Ok
  }
  
}

object Global extends GlobalSettings {
  override def onHandlerNotFound(request: RequestHeader): Result = {
    NotFound(views.html.notFound())
  }
}