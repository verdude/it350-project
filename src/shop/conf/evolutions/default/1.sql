
# --- !Ups

#CREATE DATABASE santis_onestop;

CREATE TABLE employee (
empId INT(3) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(30),
dateHired DATE,
position VARCHAR(30));

CREATE TABLE teacher (
teacherId INT(3) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
firstName VARCHAR(30),
lastName VARCHAR(30));

CREATE TABLE class (
classId INT(3) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
date_time VARCHAR(30),
name VARCHAR(30),
capacity INT(3));

CREATE TABLE student (
studentId INT(3) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
student_name VARCHAR(30),
student_phone VARCHAR(30),
student_dob DATE);

CREATE TABLE classPayment (
cPayId INT(3) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
paymentType VARCHAR(30),
amount INT(6),
pay_date DATE);

CREATE TABLE store (
storeId INT(3) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
manager VARCHAR(30),
address VARCHAR(30),
store_phone VARCHAR(30));

CREATE TABLE purchase (
purchaseId INT(3) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
purchase_date DATE);

CREATE TABLE customer (
customerId INT(3) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
customer_phone VARCHAR(30),
customer_name VARCHAR(30),
customer_addr VARCHAR(30),
customer_email VARCHAR(30));

CREATE TABLE product (
productId INT (3) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
name varchar(30),
price DECIMAL(13,2));

CREATE TABLE supplier (
supplierId INT(3) AUTO_INCREMENT PRIMARY KEY,
companyName VARCHAR(30),
supplier_addr VARCHAR(30));

CREATE TABLE inventory (
inventoryId INT(3) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
quantity INT(3));

CREATE TABLE classTeacher (
teacherId INT(3) PRIMARY KEY);

CREATE TABLE pays (
cpayId INT(3) AUTO_INCREMENT PRIMARY KEY);

CREATE TABLE purchaseLink (
purchaseId INT(3) PRIMARY KEY,
cust_phone VARCHAR(30));

# --- !Downs

DROP TABLE employee;
DROP TABLE teacher;
DROP TABLE class;
DROP TABLE student;
DROP TABLE classPayment;
DROP TABLE store;
DROP TABLE purchase;
DROP TABLE customer;
DROP TABLE product;
DROP TABLE supplier;
DROP TABLE inventory;
DROP TABLE classTeacher;
DROP TABLE pays;
DROP TABLE purchaseLink;

#DROP DATABASE santis_onestop;
