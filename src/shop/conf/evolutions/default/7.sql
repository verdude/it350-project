# --- !Ups

CREATE TRIGGER emp_trigger AFTER INSERT ON employee
FOR EACH ROW
BEGIN
INSERT INTO employee_log
   (date_time)
   VALUES
   (SYSDATE());
END;

CREATE TRIGGER teacher_trigger AFTER INSERT ON teacher
FOR EACH ROW
BEGIN
INSERT INTO teacher_log
   (date_time)
   VALUES
   (SYSDATE());
END;
