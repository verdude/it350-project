# --- !Ups

INSERT INTO store (manager, address, store_phone) VALUES ("Greg", "123 Peach St.", "555-222-5555");
INSERT INTO store (manager, address, store_phone) VALUES ("James", "321 Peach Drive", "555-111-5555");

INSERT INTO employee (name, dateHired, position, storeId) VALUES ("Greg", 20021112, "Manager", 1);
INSERT INTO employee (name, dateHired, position, storeId) VALUES ("James", 20031012, "Manager", 2);
INSERT INTO employee (name, dateHired, position, storeId) VALUES ("Bert", 20031020, "Salesman", 2);
INSERT INTO employee (name, dateHired, position, storeId) VALUES ("Holly", 20021125, "Salesman", 1);
INSERT INTO employee (name, dateHired, position, storeId) VALUES ("Joseph", 20040920, "Teacher", 1);
INSERT INTO employee (name, dateHired, position, storeId) VALUES ("Harold", 20050609, "Teacher", 2);

INSERT INTO teacher (firstName, lastName, storeId, empId) VALUES ("Joseph", "Richards", 1, 5);
INSERT INTO teacher (firstName, lastName, storeId, empId) VALUES ("Harold", "Moonshine", 2, 6);

INSERT INTO class (date_time, name, capacity, storeId, teacherId) VALUES ("10am-MWF", "Basic Guitar 101", 16, 1, 1);
INSERT INTO class (date_time, name, capacity, storeId, teacherId) VALUES ("2pm-TTh", "Drum Like a God 101", 5, 2, 2);

INSERT INTO student (student_name, student_phone, student_dob) VALUES ("Marcy", "222-111-5555", 19991015);
INSERT INTO student (student_name, student_phone, student_dob) VALUES ("Billy", "444-111-5555", 19981015);

INSERT INTO classPayment (paymentType, amount, pay_date, storeId) VALUES ("Credit", 25, 20151102, 1);
INSERT INTO classPayment (paymentType, amount, pay_date, storeId) VALUES ("Check", 25, 20151101, 2);

INSERT INTO product (name, price) VALUES ("Trumpet", 107.99);
INSERT INTO product (name, price) VALUES ("Guitar", 575.99);
INSERT INTO product (name, price) VALUES ("Drum Set", 1050.00);
INSERT INTO product (name, price) VALUES ("Bass Guitar", 478.99);
INSERT INTO product (name, price) VALUES ("Cowbell", 10.40);

INSERT INTO purchase (purchase_date, storeId, productId) VALUES (20151101, 1, 2);
INSERT INTO purchase (purchase_date, storeId, productId) VALUES (20151102, 1, 4);
INSERT INTO purchase (purchase_date, storeId, productId) VALUES (20151103, 2, 5);

INSERT INTO customer (customer_phone, customer_name, customer_addr, customer_email) VALUES ("666-777-8888", "Francis", "45 Windy St.", "francy@what.com");
INSERT INTO customer (customer_phone, customer_name, customer_addr, customer_email) VALUES ("666-111-8888", "Zane", "117 Rocker St.", "rocker@what.com");
INSERT INTO customer (customer_phone, customer_name, customer_addr, customer_email) VALUES ("666-789-8888", "Alice", "31 Bucket St.", "alice_in_chains@what.com");

INSERT INTO supplier (companyName, supplier_addr) VALUES ("Braven", "2020 Johnson Dr.");

INSERT INTO inventory (quantity, storeId, productId) VALUES (5, 1, 1);
INSERT INTO inventory (quantity, storeId, productId) VALUES (5, 1, 2);
INSERT INTO inventory (quantity, storeId, productId) VALUES (5, 1, 3);
INSERT INTO inventory (quantity, storeId, productId) VALUES (5, 1, 4);
INSERT INTO inventory (quantity, storeId, productId) VALUES (5, 1, 5);

INSERT INTO inventory (quantity, storeId, productId) VALUES (5, 2, 1);
INSERT INTO inventory (quantity, storeId, productId) VALUES (5, 2, 2);
INSERT INTO inventory (quantity, storeId, productId) VALUES (5, 2, 3);
INSERT INTO inventory (quantity, storeId, productId) VALUES (5, 2, 4);
INSERT INTO inventory (quantity, storeId, productId) VALUES (5, 2, 5);

INSERT INTO classTeacher (teacherId, classId) VALUES (1, 1);
INSERT INTO classTeacher (teacherId, classId) VALUES (2, 2);

INSERT INTO pays (studentId) VALUES (1);
INSERT INTO pays (studentId) VALUES (2);

INSERT INTO purchaseLink (purchaseId, cust_phone) VALUES (1, "666-777-8888");
INSERT INTO purchaseLink (purchaseId, cust_phone) VALUES (2, "666-111-8888");
INSERT INTO purchaseLink (purchaseId, cust_phone) VALUES (3, "666-789-8888");
