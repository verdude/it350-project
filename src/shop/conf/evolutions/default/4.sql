# --- !Ups

CREATE TABLE login (
loginId INT(3) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(30),
username VARCHAR(30),
password VARCHAR(30),
position VARCHAR(30));

# --- !Downs

DROP TABLE login;
