# --- !Ups

ALTER TABLE employee ADD storeId INT UNSIGNED NOT NULL;

ALTER TABLE employee
ADD FOREIGN KEY fk_employee(storeId)
REFERENCES store(storeId);

ALTER TABLE teacher ADD storeId INT UNSIGNED NOT NULL;
ALTER TABLE teacher ADD empId INT UNSIGNED NOT NULL;

ALTER TABLE teacher
ADD FOREIGN KEY fk_teacher1(storeId)
REFERENCES store(storeId);

ALTER TABLE teacher
ADD FOREIGN KEY fk_teacher2(empId)
REFERENCES employee(empId);

ALTER TABLE class ADD storeId INT UNSIGNED NOT NULL;
ALTER TABLE class ADD teacherId INT UNSIGNED NOT NULL;

ALTER TABLE class
ADD FOREIGN KEY fk_class(storeId)
REFERENCES store(storeId);

ALTER TABLE class
ADD FOREIGN KEY fk_class2(teacherId)
REFERENCES teacher(teacherId);

ALTER TABLE classPayment ADD storeId INT UNSIGNED NOT NULL;

ALTER TABLE classPayment
ADD FOREIGN KEY fk_classPayment(storeId)
REFERENCES store(storeId);

ALTER TABLE purchase ADD storeId INT UNSIGNED NOT NULL;
ALTER TABLE purchase ADD productId INT UNSIGNED NOT NULL;

ALTER TABLE purchase
ADD FOREIGN KEY fk_purchase1(storeId)
REFERENCES store(storeId);

ALTER TABLE purchase
ADD FOREIGN KEY fk_purchase2(productId)
REFERENCES product(productId);

ALTER TABLE inventory ADD storeId INT UNSIGNED NOT NULL;
ALTER TABLE inventory ADD productId INT UNSIGNED NOT NULL;

ALTER TABLE inventory
ADD FOREIGN KEY fk_inventory1(storeId)
REFERENCES store(storeId);

ALTER TABLE inventory
ADD FOREIGN KEY fk_inventory2(productId)
REFERENCES product(productId);

ALTER TABLE classTeacher ADD classId INT UNSIGNED NOT NULL;

ALTER TABLE classTeacher
ADD FOREIGN KEY fk_classTeacher(classId)
REFERENCES class(classId);

ALTER TABLE pays ADD studentId INT UNSIGNED NOT NULL;

ALTER TABLE pays
ADD FOREIGN KEY fk_pays(studentId)
REFERENCES student(studentId)
