# --- !Ups

CREATE TABLE employee_log (
empLogId INT(3) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
time_changed DATE);

CREATE TABLE teacher_log (
teacherLogId INT(3) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
time_changed DATE);

# --- !Downs

DROP employee_log;
DROP teacher_log;
